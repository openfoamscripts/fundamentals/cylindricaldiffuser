import re
import pyvista
import subprocess
import sys

from PyQt5.QtCore import (Qt, QFileInfo, QObject, QProcess, pyqtSignal)
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QLabel,
    QCheckBox,
    QApplication,
    QDialog,
    QTabWidget,
    QDialogButtonBox,
    QFrame,
    QGroupBox,
    QLineEdit,
    QListWidget,
    QPlainTextEdit,
    QPushButton,
    QSlider
)


class WidgetForSingleSimulation(QDialog):
    def __init__(self, file_name: str, parent: QWidget = None):
        super().__init__(parent)

        file_info = QFileInfo(file_name)

        self.geometry_tab = GeometryTab(file_info, self)
        self.flow_tab = FlowTab(file_info, self)
        self.visualization_tab = VisualizationTab(file_info, self)

        tab_widget = QTabWidget()
        tab_widget.addTab(self.geometry_tab, "  Geometry  ")
        tab_widget.addTab(self.flow_tab, "    Flow    ")
        tab_widget.addTab(self.visualization_tab, "Visualization")

        self.message_label = QLabel(self)
        self.message_label.setText("Insufficient inputs. Please check each tab.")
        self.message_label.setStyleSheet("QLabel { color : darkred; background-color : lightpink; }")

        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Close
        )

        self.button_box.setEnabled(False)
        self.button_box.accepted.connect(self.run)
        self.button_box.rejected.connect(self.reject)

        main_layout = QVBoxLayout()
        main_layout.addWidget(tab_widget)
        main_layout.addWidget(self.message_label)
        main_layout.addWidget(self.button_box)
        self.setLayout(main_layout)
        self.setWindowTitle("Settings for Single Simulation")
    
    def enable_button_box(self):
        if all([self.geometry_tab.is_complete == True, self.flow_tab.is_complete == True, self.visualization_tab.is_complete == True]):
            self.button_box.setEnabled(True)
            self.message_label.setText("Sufficient inputs. Click OK to run the simulation.")
            self.message_label.setStyleSheet("QLabel { color : darkgreen; background-color : lightgreen; }")
        else:
            self.button_box.setEnabled(False)
            self.message_label.setText("Insufficient inputs. Please check each tab.")
            self.message_label.setStyleSheet("QLabel { color : darkred; background-color : lightpink; }")
        
        # https://www.w3.org/TR/SVG11/types.html#ColorKeywords

    def run(self):
        # dummy
        print("should've print foamMonitor...")
        return


class GeometryTab(QWidget):
    is_complete = False

    def __init__(self, file_info: QFileInfo, parent: QWidget):
        super().__init__(parent)

        theta_label = QLabel("Angle:")

        self.theta_edit = QLineEdit("")
        self.theta_edit.setReadOnly(True)
        self.theta_edit.textChanged.connect(self.change_status)
        self.theta_edit.textChanged.connect(parent.enable_button_box)

        self.theta_slider = QSlider(Qt.Horizontal, self)
        self.theta_slider.setFocusPolicy(Qt.StrongFocus)
        self.theta_slider.setRange(10, 180)
        self.theta_slider.valueChanged.connect(self.change_value)
        self.theta_slider.setTickPosition(QSlider.TicksBothSides)
        self.theta_slider.setTickInterval(10)
        self.theta_slider.setSingleStep(5)
        self.theta_slider.setPageStep(1)        

        freecad_button = QPushButton("Check the geometry with FreeCAD")
        freecad_button.clicked.connect(self.run_freecad, Qt.QueuedConnection)
    
        process = Process()
        stdout_text = QPlainTextEdit()
        process.stdout.connect(stdout_text.appendPlainText)

        main_layout = QVBoxLayout()
        main_layout.addWidget(theta_label)
        main_layout.addWidget(self.theta_edit)
        main_layout.addWidget(self.theta_slider)
        # main_layout.addStretch(1)
        main_layout.addWidget(freecad_button)
        # main_layout.addStretch(1)
        main_layout.addWidget(stdout_text)
        self.setLayout(main_layout)


    def change_status(self):
        if self.theta_edit.text() == '':
            self.is_complete = False
        else:
            self.is_complete = True


    def change_value(self):
        current_value = self.theta_slider.value()
        self.theta_edit.setText(str(current_value))
        return


    def run_freecad(self):
        file = "./geometricProperties"
        key = "geometriesForFreeCAD.taper_angle"
        angle = self.theta_slider.value()
        subprocess.run(['foamDictionary "' + file + '" -entry "' + key + '" -set "' + str(angle) + '" -disableFunctionEntries'], shell=True)
        subprocess.run(["cd _1_geometry; ./run.bash"], shell=True)

        stlfile = '_1_geometry/' + self.get_property("fileNames.triSurface_meter") + '.stl'
        mesh = pyvista.read(stlfile)
        mesh.plot(show_edges=True)
        return


    def get_property(self, key):
        file = "./geometricProperties"
        foam_dict = subprocess.run(['foamDictionary ' + file + ' -entry "' + key + '" -value'], shell=True, encoding='utf_8', stdout=subprocess.PIPE)
        _stdout = foam_dict.stdout

        if re.match("^[+\-]?[0-9]", _stdout) == None:
            # print("value type : str")
            value = _stdout.replace('\n', '').replace('"', '')
        elif re.match("^[+\-]?[0-9]+\.", _stdout) == None:
            # print("value type : int")
            value = int(_stdout)
        else:
            # print("value type : float")
            value = float(_stdout)

        return value


class Process(QObject):

    stdout = pyqtSignal(str)
    stderr = pyqtSignal(str)
    finished = pyqtSignal(int)

    def start(self, program, args):
        process = QProcess()
        process.setProgram(program)
        process.setArguments(args)
        process.readyReadStandardError.connect(lambda: self.stderr.emit(buffer_to_str(process.readAllStandardError())))
        process.readyReadStandardOutput.connect(lambda: self.stderr.emit(buffer_to_str(process.readAllStandardOutput())))
        process.finished.connect(self.finished)
        process.start()
        
        self._process = process


class FlowTab(QWidget):
    # is_complete = False
    is_complete = True

    def __init__(self, file_info: QFileInfo, parent: QWidget):
        super().__init__(parent)

        permissions_group = QGroupBox("Permissions")

        readable = QCheckBox("Readable")
        if file_info.isReadable():
            readable.setChecked(True)

        writable = QCheckBox("Writable")
        if file_info.isWritable():
            writable.setChecked(True)

        executable = QCheckBox("Executable")
        if file_info.isExecutable():
            executable.setChecked(True)

        owner_group = QGroupBox("Ownership")

        owner_label = QLabel("Owner")
        owner_value_label = QLabel(file_info.owner())
        owner_value_label.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        group_label = QLabel("Group")
        group_value_label = QLabel(file_info.group())
        group_value_label.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        permissions_layout = QVBoxLayout()
        permissions_layout.addWidget(readable)
        permissions_layout.addWidget(writable)
        permissions_layout.addWidget(executable)
        permissions_group.setLayout(permissions_layout)

        owner_layout = QVBoxLayout()
        owner_layout.addWidget(owner_label)
        owner_layout.addWidget(owner_value_label)
        owner_layout.addWidget(group_label)
        owner_layout.addWidget(group_value_label)
        owner_group.setLayout(owner_layout)

        main_layout = QVBoxLayout()
        main_layout.addWidget(permissions_group)
        main_layout.addWidget(owner_group)
        main_layout.addStretch(1)
        self.setLayout(main_layout)


class VisualizationTab(QWidget):
    is_complete = False

    def __init__(self, file_info: QFileInfo, parent: QWidget):
        super().__init__(parent)

        top_label = QLabel("Open with:")

        applications_list_box = QListWidget()
        applications = []

        for i in range(1, 31):
            applications.append(f"Application {i}")
        applications_list_box.insertItems(0, applications)

        if not file_info.suffix():
            self.always_check_box = QCheckBox(
                "Always use this application to open this type of file"
            )
        else:
            self.always_check_box = QCheckBox(
                f"Always use this application to open files "
                f"with the extension {file_info.suffix()}"
            )
        self.always_check_box.stateChanged.connect(self.change_status)
        self.always_check_box.stateChanged.connect(parent.enable_button_box)

        layout = QVBoxLayout()
        layout.addWidget(top_label)
        layout.addWidget(applications_list_box)
        layout.addWidget(self.always_check_box)
        self.setLayout(layout)
    
    def change_status(self):
        if self.always_check_box.checkState() == Qt.Checked:
            self.is_complete = True
        else:
            self.is_complete = False


if __name__ == "__main__":
    app = QApplication(sys.argv)

    if len(sys.argv) >= 2:
        file_name = sys.argv[1]
    else:
        file_name = "."

    tab_dialog = WidgetForSingleSimulation(file_name)
    tab_dialog.setStyleSheet("""
        font-family: Hack;
        font-size: 18px;
        """)
    tab_dialog.show()

    sys.exit(app.exec())