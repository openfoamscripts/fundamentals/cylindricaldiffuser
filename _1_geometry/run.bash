#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

# set command name ('freecadcmd' for deb package and 'FreeCADCmd' for rpm package)
if [ -d /etc/apt ]; then
    freecadcmd='freecadcmd'
elif [ -d /etc/dnf ]; then
    freecadcmd='FreeCADCmd'
elif [ -d /etc/zypp ]; then
    freecadcmd='FreeCADCmd'
else
    freecadcmd='FreeCADCmd'
fi

# create solid body
$freecadcmd script_pre.py

# 1. downgrade the body into faces
# 2. create surface meshes from the faces
# 3. export the meshes as stl file
$freecadcmd script.py

# Now, we get the stl file for OpenFOAM simulation and every patch is named there.
# To use cfMesh, fms file is exported as well.
