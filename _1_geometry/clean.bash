#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

rm *.FCStd *.FCStd1
rm *.stl *.fms
rm -r __pycache__
