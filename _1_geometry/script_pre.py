import os
import FreeCAD as App
import Part
import Sketcher

import foam_dictionary_reader as fdr


class ConicalDiffuser_Solid():
    X_AXIS = -1
    Y_AXIS = -2
    

    def __init__(self, _name):
        print("begin constructer")
        self.name = _name
        self.doc = App.newDocument(self.name)

        self.bodies = []
        self.sketchs = []
        self.solids = []

        self.bodies.append(self.doc.addObject('PartDesign::Body', 'Body000'))

        # read parameters
        self.DIAMETER_1 = fdr.get_geometry_property("geometriesForFreeCAD.diameter_1")
        self.DIAMETER_2 = fdr.get_geometry_property("geometriesForFreeCAD.diameter_2")
        self.TAPER_ANGLE = fdr.get_geometry_property("geometriesForFreeCAD.taper_angle")
        self.SMALL_LENGTH = fdr.get_geometry_property("geometriesForFreeCAD.small_length")

        print("end constructer")
        return


    def create_base_solid(self, sketchlabel, solidlabel):
        self.sketchs.append(self.bodies[0].newObject('Sketcher::SketchObject', sketchlabel))
        sketch = self.sketchs[-1]
        sketch.Support = (self.doc.getObject('XY_Plane'), [''])
        sketch.MapMode = 'FlatFace'

        geo_list = []
        # index 0 ~ 4
        geo_list.append(Part.LineSegment(App.Vector(0.0, 25.0, 0.0), App.Vector(-5.0, 25.0, 0.0)))
        geo_list.append(Part.LineSegment(App.Vector(-5.0, 25.0, 0.0), App.Vector(-5.0, 0.0, 0.0)))
        geo_list.append(Part.LineSegment(App.Vector(-5.0, 0.0, 0.0), App.Vector(100.0, 0.0, 0.0)))
        geo_list.append(Part.LineSegment(App.Vector(100.0, 0.0, 0.0), App.Vector(100.0, 50.0, 0.0)))
        geo_list.append(Part.LineSegment(App.Vector(100.0, 50.0, 0.0), App.Vector(95.0, 50.0, 0.0)))
        # index 5
        geo_list.append(Part.LineSegment(App.Vector(95.0, 50.0, 0.0), App.Vector(0.0, 25.0, 0.0)))

        geo = sketch.addGeometry(geo_list, False)

        con_list = []
        # index 0 ~ 4
        con_list.append(Sketcher.Constraint('Coincident', geo[1], 1, geo[0], 2))
        con_list.append(Sketcher.Constraint('Coincident', geo[2], 1, geo[1], 2))
        con_list.append(Sketcher.Constraint('Coincident', geo[3], 1, geo[2], 2))
        con_list.append(Sketcher.Constraint('Coincident', geo[4], 1, geo[3], 2))
        con_list.append(Sketcher.Constraint('Coincident', geo[5], 1, geo[4], 2))
        # index 5 ~ 9
        con_list.append(Sketcher.Constraint('Coincident', geo[0], 1, geo[5], 2))
        con_list.append(Sketcher.Constraint('Horizontal', geo[0]))
        con_list.append(Sketcher.Constraint('Vertical', geo[1]))
        con_list.append(Sketcher.Constraint('Horizontal', geo[2]))
        con_list.append(Sketcher.Constraint('Vertical', geo[3]))
        # index 10 ~ 14
        con_list.append(Sketcher.Constraint('Horizontal', geo[4]))
        con_list.append(Sketcher.Constraint('PointOnObject', geo[0], 1, self.Y_AXIS))
        con_list.append(Sketcher.Constraint('PointOnObject', geo[1], 2, self.X_AXIS))
        con_list.append(Sketcher.Constraint('DistanceY', geo[1], 2, geo[1], 1, 25.0))
        con_list.append(Sketcher.Constraint('DistanceY', geo[3], 1, geo[3], 2, 50.0))
        # index 15 ~ 17
        con_list.append(Sketcher.Constraint('DistanceX', geo[0], 2, geo[0], 1, 5.0))
        con_list.append(Sketcher.Constraint('DistanceX', geo[4], 2, geo[4], 1, 5.0))
        con_list.append(Sketcher.Constraint('Angle', geo[2], 1, geo[5], 2, 0.31416))  # rad

        con = sketch.addConstraint(con_list)

        # note: constraint of DIAMETER_2 MUST BE defined before that of DIAMETER_1
        #       because DIAMETER_2 > DIAMETER_1. (Or the sketch solver may fail to update geometry.)
        sketch.setDatum(con[14], App.Units.Quantity(str(0.5 * self.DIAMETER_2) + ' mm'))
        sketch.setDatum(con[13], App.Units.Quantity(str(0.5 * self.DIAMETER_1) + ' mm'))
        sketch.setDatum(con[15], App.Units.Quantity(str(self.SMALL_LENGTH) + ' mm'))
        sketch.setDatum(con[16], App.Units.Quantity(str(self.SMALL_LENGTH) + ' mm'))
        sketch.setDatum(con[17], App.Units.Quantity(str(0.5 * self.TAPER_ANGLE) + ' deg'))

        del geo_list, con_list    

        self.solids.append(self.bodies[0].newObject('PartDesign::Revolution', solidlabel))
        solid = self.solids[-1]
        solid.Profile = sketch
        solid.ReferenceAxis = (self.doc.getObject('X_Axis'), [''])
        solid.Angle = 360.0
        self.doc.recompute()
        return 


    def extend_solid(self, sketchlabel, solidlabel, minmax, length): # 'solid001', 'Sketch001'
        # get solid and face index to create the sketch on it
        x_of_faces = []
        faces_of_solid = self.solids[-1].Shape.Faces
        for i in range(len(faces_of_solid)):
            face = faces_of_solid[i]
            x_of_faces.append(face.CenterOfMass.x)
        
        match minmax:
            case "min":
                value = min(x_of_faces)
            case "max":
                value = max(x_of_faces)
                
        index_faces = x_of_faces.index(value)
        print("index_faces: " + str(index_faces))

        # create a new sketch
        self.sketchs.append(self.bodies[0].newObject('Sketcher::SketchObject', sketchlabel))
        sketch = self.sketchs[-1]
        sketch.Support = (self.solids[-1], 'Face' + str(index_faces + 1))
        sketch.MapMode = 'FlatFace'

        # get solid and edge index to refer from the sketch circle
        x_of_edges = []
        edges_of_solid = self.solids[-1].Shape.Edges
        for i in range(len(edges_of_solid)):
            edge = edges_of_solid[i]
            x_of_edges.append(edge.CenterOfMass.x)
        
        match minmax:
            case "min":
                value = min(x_of_edges)
            case "max":
                value = max(x_of_edges)

        index_edges = x_of_edges.index(value)
        print("index_edges: " + str(index_edges))

        # create externals, geometries and constraints
        ext = []
        sketch.addExternal(self.solids[-1].Label, "Edge" + str(index_edges + 1))
        ext.append(-3) # index of addExternal goes -3, -4, -5,... ("-1" represents X axis and "-2" does Y axis)
        print(str(ext))

        geo_list = []
        # index 0
        geo_list.append(Part.Circle(App.Vector(22.77, 16.09, 0), App.Vector(0, 0, 1), 38.15))
        geo = sketch.addGeometry(geo_list, False)

        con_list = []
        # index 0 ~ 1
        con_list.append(Sketcher.Constraint('Coincident', geo[0], 3, ext[0], 3)) # 3 represents center of circle?
        con_list.append(Sketcher.Constraint('Equal', geo[0], ext[0]))
        con = sketch.addConstraint(con_list)

        del geo_list, con_list

        self.solids.append(self.bodies[0].newObject('PartDesign::Pad', solidlabel))
        solid = self.solids[-1]
        solid.Profile = sketch
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.Length = length
        solid.TaperAngle = 0.0
        solid.UseCustomVector = 0
        solid.Direction = (0, 0, 1)
        solid.AlongSketchNormal = 1
        solid.Type = 0
        solid.UpToFace = None
        solid.Reversed = 0
        solid.Midplane = 0
        solid.Offset = 0

        self.doc.recompute()
    

    def trim_three_fourths_of_body(self, sketchlabel, solidlabel):
        # Remove 3/4 of the whole body (to reduce computational cost in CFD)
        self.sketchs.append(self.bodies[0].newObject('Sketcher::SketchObject', sketchlabel))
        sketch = self.sketchs[-1]
        sketch.Support = (self.doc.getObject('YZ_Plane'), [''])
        sketch.MapMode = 'FlatFace'

        geo_list = []
        # index 0 ~ 2
        geo_list.append(Part.Circle(App.Vector(0.0, 0.0, 0.0), App.Vector(0, 0, 1), 150.0))
        geo_list.append(Part.LineSegment(App.Vector(0.0, 0.0, 0), App.Vector(0.0, 150.0, 0.0)))
        geo_list.append(Part.LineSegment(App.Vector(0.0, 0.0, 0), App.Vector(150.0, 0.0, 0.0)))

        geo = sketch.addGeometry(geo_list, False)

        con_list = []
        # index 0 ~ 4
        con_list.append(Sketcher.Constraint('Coincident', geo[0], 3, self.X_AXIS, 1))
        con_list.append(Sketcher.Constraint('Coincident', geo[1], 1, geo[0], 3))
        con_list.append(Sketcher.Constraint('PointOnObject', geo[1], 2, geo[0]))
        con_list.append(Sketcher.Constraint('Vertical', geo[1]))
        con_list.append(Sketcher.Constraint('Coincident', geo[2], 1, geo[0], 3))
        # index 5 ~ 7
        con_list.append(Sketcher.Constraint('PointOnObject', geo[2], 2, geo[0]))
        con_list.append(Sketcher.Constraint('Horizontal', geo[2]))
        con_list.append(Sketcher.Constraint('Radius', geo[0], 50.0))

        con_3 = sketch.addConstraint(con_list)

        del geo_list, con_list

        sketch.setDatum(con_3[7], App.Units.Quantity(str(0.5 * self.DIAMETER_2) + ' mm'))
        # a point on the first quadrant of the circle (using Pythagorean theorem)
        sketch.trim(geo[0], App.Vector(0.4 * self.DIAMETER_2, 0.3 * self.DIAMETER_2, 0.0))

        self.solids.append(self.bodies[0].newObject('PartDesign::Pocket', solidlabel))
        solid = self.solids[-1]
        solid.Profile = sketch
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.AlongSketchNormal = 1
        solid.Type = 1  # boring through
        solid.Midplane = 1  # both side

        self.doc.recompute()
        return


    def save(self):
        pwd = os.getcwd()
        file_path = pwd + "/" + self.name + ".FCStd"
        self.doc.saveAs(file_path)
        return


if __name__ == 'script_pre':
    DOC_NAME_0 = fdr.get_geometry_property("fileNames.FreeCADDoc")
 
    cad = ConicalDiffuser_Solid(DOC_NAME_0)
    cad.create_base_solid("Sketch000", "Solid000_Revolution")

    SMALL_LENGTH = fdr.get_geometry_property("geometriesForFreeCAD.small_length")
    cad.extend_solid("Sketch001", "Solid001_Pad", "min", SMALL_LENGTH)
    cad.extend_solid("Sketch002", "Solid002_Pad", "max", SMALL_LENGTH)

    STRAIGHT_LENGTH_INLET = fdr.get_geometry_property("geometriesForFreeCAD.straight_length_inlet")
    STRAIGHT_LENGTH_OUTLET = fdr.get_geometry_property("geometriesForFreeCAD.straight_length_outlet")
    cad.extend_solid("Sketch003", "Solid003_Pad", "min", STRAIGHT_LENGTH_INLET)
    cad.extend_solid("Sketch004", "Solid004_Pad", "max", STRAIGHT_LENGTH_OUTLET)

    cad.trim_three_fourths_of_body("Sketch005", "Solid005_Pocket")
    cad.save()
