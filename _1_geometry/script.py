import os
import subprocess
import FreeCAD as App
import Draft
import Mesh
import MeshPart

import foam_dictionary_reader as fdr


class ConicalDiffuser_Faces():
    def __init__(self, _name):
        print("begin constructer")
        self.name = _name
        pwd = os.getcwd()
        self.file_path = pwd + "/" + self.name + ".FCStd"
        self.doc = App.open(self.file_path)

        self.solid = self.doc.ActiveObject

        self.faces = {}
        self.meshes = {}

        # read parameters
        self.X_INLET = fdr.get_geometry_property("geometriesForFreeCAD.x_inlet")
        self.X_WALL_UP = fdr.get_geometry_property("geometriesForFreeCAD.x_wall_up")
        self.X_WALL_DOWN = fdr.get_geometry_property("geometriesForFreeCAD.x_wall_down")
        self.X_OUTLET = fdr.get_geometry_property("geometriesForFreeCAD.x_outlet")

        self.STL_MILLIMETER_LABEL = fdr.get_geometry_property("fileNames.triSurface_millimeter")
        self.STL_METER_LABEL = fdr.get_geometry_property("fileNames.triSurface_meter")

        print("end constructer")
        return


    def set_facelabel(self):
        faces_originallabel = Draft.downgrade(self.solid, delete=False)[0]

        TOLERANCE = 1.0e-3  # [mm]

        faces_wall = []
        faces_symmetry = []

        for face in faces_originallabel:
            x = face.Shape.CenterOfMass.x
            y = face.Shape.CenterOfMass.y
            z = face.Shape.CenterOfMass.z
            if abs(y) < TOLERANCE:
                print("_symmetry")
                faces_symmetry.append(face)
            elif abs(z) < TOLERANCE:
                print("_symmetry")
                faces_symmetry.append(face)
            elif abs(x - self.X_INLET) < TOLERANCE:
                print("_inlet")
                face_inlet = face
                face_inlet.Label = '_inlet'
            elif abs(x - self.X_WALL_UP) < TOLERANCE:
                print("_wall_upstream_of_taper")
                face_wall_up = face
                face_wall_up.Label = '_wall_upstream_of_taper'
            elif abs(x - self.X_WALL_DOWN) < TOLERANCE:
                print("_wall_downstream_of_taper")
                face_wall_down = face
                face_wall_down.Label = '_wall_downstream_of_taper'
            elif abs(x - self.X_OUTLET) < TOLERANCE:
                print("_outlet")
                face_outlet = face
                face_outlet.Label = '_outlet'
            else:
                print("_wall")
                faces_wall.append(face)

        face_wall = Draft.upgrade(faces_wall, delete=True)[0][0]
        face_wall.Label = '_wall'

        face_symmetry = Draft.upgrade(faces_symmetry, delete=True)[0][0]
        face_symmetry.Label = '_symmetry'

        self.faces = {'inlet': face_inlet, 'outlet': face_outlet, 'wall_upstream_of_taper': face_wall_up,
                        'wall_downstream_of_taper': face_wall_down, 'wall_others': face_wall, 'symmetry': face_symmetry}
        
        self.doc.recompute()
        return


    def export_stl(self):
        self.meshes = self.faces.copy()
        for key in self.faces.keys():
            face = self.faces[key]
            self.meshes[key] = self.doc.addObject("Mesh::Feature", key)
            # want to replace default mesher by gmsh in the future:
            self.meshes[key].Mesh = MeshPart.meshFromShape(face.Shape, MaxLength=5.0)
            Mesh.export([self.meshes[key]], key + '.ast')

        return


    def cat_stl(self):
        for key in self.meshes.keys():
            subprocess.run(["sed -i 's/Mesh/" + key + "/g' " + key + ".ast"], shell=True)

        subprocess.run(['cat *.ast > ' + self.STL_MILLIMETER_LABEL + '.stl'], shell=True)
        # note: using OpenFOAM commands
        subprocess.run(['surfaceConvert -scale 0.001 ' + self.STL_MILLIMETER_LABEL + '.stl ' + self.STL_METER_LABEL + '.stl'], shell=True)
        subprocess.run(['surfaceFeatureEdges -angle 10 ' + self.STL_METER_LABEL + '.stl ' + self.STL_METER_LABEL + '.fms'], shell=True)
        subprocess.run(['rm *.ast ' + self.STL_MILLIMETER_LABEL + '.stl'], shell=True)

        return
    

    def save(self):
        self.doc.saveAs(self.file_path)
        return


if __name__ == 'script':
    DOC_NAME = fdr.get_geometry_property("fileNames.FreeCADDoc")

    cad = ConicalDiffuser_Faces(DOC_NAME)
    cad.set_facelabel()
    cad.export_stl()
    cad.cat_stl()
    cad.save()
