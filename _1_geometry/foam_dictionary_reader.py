import re
import subprocess



def get_geometry_property(key):
    # Todo: detect int beside float and str
    foam_dict = subprocess.run(['foamDictionary "../geometricProperties" -entry "' + key + '" -value'], shell=True, encoding='utf_8', stdout=subprocess.PIPE)
    _stdout = foam_dict.stdout

    if re.match("^[+\-]?[0-9]", _stdout) == None:
        # print("value type : str")
        value = _stdout.replace('\n', '').replace('"', '')
    elif re.match("^[+\-]?[0-9]+\.", _stdout) == None:
        # print("value type : int")
        value = int(_stdout)
    else:
        # print("value type : float")
        value = float(_stdout)

    return value
