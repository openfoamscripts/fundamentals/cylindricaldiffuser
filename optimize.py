import os
import subprocess
import optuna
import logging
import matplotlib.pyplot as plt

from _3_postprocess.extract_values import c_extract_values


def all_run(angle):
    file = "./geometricProperties"
    key = "geometriesForFreeCAD.taper_angle"
    subprocess.run(['foamDictionary "' + file + '" -entry "' + key + '" -set "' + str(angle) + '" -disableFunctionEntries'], shell=True)
    # subprocess.run(['foamDictionary ./geometricProperties -entry "' + key + '" -set "' + str(angle) + '" -disableFunctionEntries'], shell=True)
    subprocess.run(['./Allrun'], shell=True)
    return


def all_clean():
    subprocess.run(['./Allclean'], shell=True)
    return


def archive_flow_figure(num):
    subprocess.run(['cp ./_3_postprocess/U.png ./archive_flow_figures/U' + str(num).zfill(3) + '.png'], shell=True)
    return


def objective(trial):
    """
    @theta angle of diffuser
    """
    all_clean()
    angle = trial.suggest_uniform('divergence_angle', 10, 180)
    all_run(angle)
    archive_flow_figure(trial.number)
    e = c_extract_values()
    xi = e.compute_pressure_drop_coefficient()

    return xi


def export_optimized_value_graph(dataframe):
    plt.figure()
    dataframe.plot(x="params_divergence_angle", y="value", style='.')
    plt.xlim(0, 180)
    plt.ylim(0.0, 1.4)
    # plt.ylim(0.0, 0.3)
    plt.xlabel("divergence angle [deg]")
    plt.ylabel("pressure loss coefficient xi [-]")
    plt.grid()
    pwd = os.getcwd()
    plt.savefig(pwd + '/fig.png')
    plt.close()
    

if __name__ == '__main__':
    subprocess.run(['rm log.optuna'], shell=True)
    subprocess.run(['rm *.db'], shell=True)
    optuna.logging.get_logger("optuna").addHandler(logging.FileHandler('./log.optuna'))

    study_name = "conicalDiffuser-pressureLossCoefficient"
    storage_name = "sqlite:///{}.db".format(study_name)
    study = optuna.create_study(direction='maximize', study_name=study_name, storage=storage_name)
    # study = optuna.create_study(direction='maximize', study_name=study_name, storage=storage_name, load_if_exists=True)
    study.optimize(objective, n_trials=50)

    print(study.best_params)
    print(study.best_trial)

    df = study.trials_dataframe(attrs=("number", "value", "params"))
    export_optimized_value_graph(df)
