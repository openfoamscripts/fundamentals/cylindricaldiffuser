from tkinter import Grid
import optuna
import os
from matplotlib import pyplot as plt
# import pandas


study_name = "conicalDiffuser-pressureLossCoefficient"
storage_name = "sqlite:///{}.db".format(study_name)
study = optuna.create_study(direction='maximize', study_name=study_name, storage=storage_name, load_if_exists=True)

# df = study.trials_dataframe()
df = study.trials_dataframe(attrs=("number", "value", "params"))
print(df)

plt.figure()
df.plot(x="params_divergence_angle", y="value", style='.')
plt.xlim(0, 180)
# plt.ylim(0.0, 1.4)
plt.ylim(-3.0, 0.2)
plt.xlabel("divergence angle [deg]")
plt.ylabel("pressure loss coefficient xi [-]")
plt.grid()
pwd = os.getcwd()
plt.savefig(pwd + '/fig.png')
plt.close()

# print(study.best_params)
# print(study.best_trial)
