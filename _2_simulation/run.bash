#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

touch paraview.foam

# WANNA USE CONST VARIABLE (e.g. "source constants.bash")
cp ../_1_geometry/CylindricalDiffuser_SI.fms .

restore0Dir

runApplication  cartesianMesh     # (cartesianMesh|tetMesh|pMesh)
runApplication  setExprBoundaryFields
runApplication  potentialFoam -writep

runApplication  decomposePar
./setSchemes_stable
runParallel     $(getApplication)

mv log.$(getApplication) log0.$(getApplication)
./setSchemes_accurate
runParallel     $(getApplication)


#------------------------------------------------------------------------------
