#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

# foamCleanTutorials 
# write as above, and get this error:
# /usr/lib/openfoam/openfoam2112/bin/foamCleanTutorials: 144: Cannot fork

rm log.* log0.* *.fms *.foam
rm -r processor* postProcessing 0 dynamicCode
rm -r constant/polyMesh

#------------------------------------------------------------------------------
