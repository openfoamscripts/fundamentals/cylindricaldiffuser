# ConicalDiffuser

Calculate flow through a conical diffuser and its pressure loss coefficient.  

"Allrun" conducts these operations:

* create STL and FMS files from scratch by using FreeCADCmd.

* create mesh in the FMS file and solve the flow.

* calculate the pressure loss coefficient

* create paraview script and paraview state file.
 

## Pressure Loss Coefficient $\xi$

```math
\begin{aligned}
    \xi &\equiv \frac{ p_{loss} }{ \frac{1}{2} \rho (\bar{u}_1 - \bar{u}_2)^2 } \\

    p_{loss} &\equiv \Delta p_{ideal} - \Delta p_{actual} \\

    \Delta p_{ideal} &\equiv \frac{1}{2} \rho (\bar{u}_1^2 - \bar{u}_2^2) \\

    \Delta p_{actual} &\equiv \tilde{p}_2 - \tilde{p}_1 \\

    \bar{u}_i &\equiv \frac{1}{A} \int_A u_i \mathrm{d} A 
    \qquad (A \text{ is the area of cross-section }i) \\

    \tilde{p}_i &\equiv \frac{1}{C} \int_C p_i \mathrm{d} s 
    \qquad (C \text{ is the coutour of cross-section }i)
\end{aligned}
```

### see also:
* Fig. 8 of the article https://www.researchgate.net/publication/226778190_Synthesis_and_post-processing_of_nanomaterials_using_microreaction_technology


## Requirement

* Linux (Debian, RHEL, SUSE based distro is OK. the others are out of consideration)
* FreeCAD (v0.20 is used)
* OpenFOAM (v2206 is used)


## How To Use

* download this project folder and extruct it in some suitable place.
* open terminal in that folder and type ```./Allrun``` and all procedure will be executed.
* to clean up the folder, type ```./Allclean```


## Derived from:

* https://gitlab.com/freecadscripts/exportstl_cylindricaldiffuser


# Setup memo

```
conda create -n foam
conda activate foam
conda install -c conda-forge freecad
conda install -c conda-forge optuna
#conda install -c conda-forge scipy
conda install -c conda-forge pandas
conda install -c conda-forge matplotlib
#conda install -c conda-forge seaborn
conda install -c conda-forge pyqt
conda install -c conda-forge pyvista
```


# UML

## Use case

## Sequence

```plantuml
loop optuna.create_study().optimize()
    optimize.py -> rootDir : all_clean()
    rootDir -> _1_geometry : clean
    rootDir -> _2_simulation : clean
    rootDir -> _3_postprocess : clean
    optimize.py -> rootDir : all_run(angle)

    == Create geometry file ==
    rootDir -> _1_geometry : run
    _1_geometry -> _1_geometry : FreeCADCmd script_pre.py
    _1_geometry -> _1_geometry : FreeCADCmd script.py

    == Run simultion ==
    rootDir -> _2_simulation : run
    _2_simulation -> _1_geometry : copy fms file
    _2_simulation -> _2_simulation : restore0Dir
    _2_simulation -> _2_simulation : cartesianMesh
    _2_simulation -> _2_simulation : setExprBoundaryFields
    _2_simulation -> _2_simulation : potentialFoam
    _2_simulation -> _2_simulation : decomposePar
    _2_simulation -> _2_simulation : setSchemes_stable
    _2_simulation -> _2_simulation : simpleFoam
    _2_simulation -> _2_simulation : setSchemes_accurate
    _2_simulation -> _2_simulation : simpleFoam

    == Postprocessing ==
    rootDir -> _3_postprocess : run
    _3_postprocess -> _3_postprocess :pvpython visualize.py
    
    optimize.py -> optimize.py : archive_flow_figure()
    optimize.py -> _3_postprocess : compute_pressure_drop_coefficient()
end

optimize.py -> optimize.py : export_optimized_value_graph()
```
