#!/bin/bash
cd "${0%/*}" || exit                                # Run from this directory
#------------------------------------------------------------------------------

foamDictionary "testDictionary" -entry "float.summation" -value

#foamDictionary "testDictionary" -entry "string.extension" -value
foamDictionary "testDictionary" -entry "string.fileName" -value
