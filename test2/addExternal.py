import os
import FreeCAD as App
import Part
import Sketcher


class FreeCAD_cylinder():
    def __init__(self, _name):
        print("begin constructer")
        self.name = _name
        self.doc = App.newDocument(self.name)

        self.bodies = []
        self.sketchs = []
        self.solids = []

        self.bodies.append(self.doc.addObject('PartDesign::Body', 'body000'))

        print("end constructer")
        return


    def create_base_solid(self):
        self.sketchs.append(self.bodies[0].newObject('Sketcher::SketchObject', 'Sketch000'))
        sketch = self.sketchs[-1]
        sketch.Support = (self.doc.getObject('XY_Plane'), [''])
        sketch.MapMode = 'FlatFace'
        sketch.addGeometry(Part.Circle(App.Vector(0.0, 0.0, 0.0), App.Vector(0, 0, 1), 100.0), False)
        sketch.addConstraint(Sketcher.Constraint('Coincident', 0, 3, -1, 1))
        sketch.addConstraint(Sketcher.Constraint('Diameter', 0, 100.0))
        sketch.setDatum(1, App.Units.Quantity('100.0 mm'))

        self.solids.append(self.bodies[0].newObject('PartDesign::Pad', 'solid000'))
        solid = self.solids[-1]
        solid.Profile = sketch
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.Length = 100.000000
        solid.TaperAngle = 0.000000
        solid.UseCustomVector = 0
        solid.Direction = (0, 0, 1)
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.AlongSketchNormal = 1
        solid.Type = 0
        solid.UpToFace = None
        solid.Reversed = 0
        solid.Midplane = 0
        solid.Offset = 0
        self.doc.recompute()
        return 


    def extend_solid(self, sketchlabel, solidlabel): # 'solid001', 'Sketch001'
        # get solid and face index to create the sketch on it
        z_of_faces = []
        for j in range(len(self.solids)):
            solid = self.solids[j]
            faces_of_solid = solid.Shape.Faces
            z_of_faces.append([])
            for i in range(len(faces_of_solid)):
                face = faces_of_solid[i]
                z_of_faces[j].append(face.CenterOfMass.z)
        
        max_value = max(z_of_faces)
        index_solids = z_of_faces.index(max_value)
        print("index_solids: " + str(index_solids))

        max_value = max(z_of_faces[index_solids])
        index_faces = z_of_faces[index_solids].index(max_value)
        print("index_faces: " + str(index_faces))

        # create a new sketch
        self.sketchs.append(self.bodies[0].newObject('Sketcher::SketchObject', sketchlabel))
        sketch = self.sketchs[-1]
        sketch.Support = (self.solids[index_solids], 'Face' + str(index_faces + 1))
        sketch.MapMode = 'FlatFace'

        # get solid and edge index to refer from the sketch circle
        z_of_edges = []
        for j in range(len(self.solids)):
            solid = self.solids[j]
            edges_of_solid = solid.Shape.Edges
            z_of_edges.append([])
            for i in range(len(edges_of_solid)):
                edge = edges_of_solid[i]
                z_of_edges[j].append(edge.CenterOfMass.z)
        
        max_value = max(z_of_edges)
        index_solids = z_of_edges.index(max_value)
        print("index_solids: " + str(index_solids))

        max_value = max(z_of_edges[index_solids])
        index_edges = z_of_edges[index_solids].index(max_value)
        print("index_edges: " + str(index_edges))

        # create externals, geometries and constraints
        ext = []
        sketch.addExternal(self.solids[index_solids].Label, "Edge" + str(index_edges + 1))
        ext.append(-3) # index of addExternal goes -3, -4, -5,... ("-1" represents X axis and "-2" does Y axis)
        print(str(ext))

        geo_list = []
        # index 0
        geo_list.append(Part.Circle(App.Vector(22.77, 16.09, 0), App.Vector(0, 0, 1), 38.15))
        geo = sketch.addGeometry(geo_list, False)

        con_list = []
        # index 0 ~ 1
        con_list.append(Sketcher.Constraint('Coincident', geo[0], 3, ext[0], 3)) # 3 represents center of circle?
        con_list.append(Sketcher.Constraint('Equal', geo[0], ext[0]))
        con = sketch.addConstraint(con_list)

        del geo_list, con_list

        self.solids.append(self.bodies[0].newObject('PartDesign::Pad', solidlabel))
        solid = self.solids[-1]
        solid.Profile = sketch
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.Length = 50.0
        solid.TaperAngle = 0.0
        solid.UseCustomVector = 0
        solid.Direction = (0, 0, 1)
        solid.ReferenceAxis = (sketch, ['N_Axis'])
        solid.AlongSketchNormal = 1
        solid.Type = 0
        solid.UpToFace = None
        solid.Reversed = 0
        solid.Midplane = 0
        solid.Offset = 0
        self.doc.recompute()


    def save(self):
        pwd = os.getcwd()
        file_path = pwd + "/" + self.name + ".FCStd"
        self.doc.saveAs(file_path)
        return


if __name__ == 'addExternal':
    cad = FreeCAD_cylinder("addExternal")
    cad.create_base_solid()
    cad.extend_solid("Sketch001", "Pad001")
    cad.extend_solid("Sketch002", "Pad002")
    cad.save()
