import os
import re
import subprocess
import pandas as pd

class c_extract_values:
    def __init__(self):
        print("__name__ = " + __name__)        
        self.pwd = os.path.dirname(__file__)
        print("extract_value: pwd = " + self.pwd)

        self.end_time = self.get_property("control.endTime.stable")
        self.rho = self.get_property("water.rho")

        return


    def get_property(self, key):
        foam_dict = subprocess.run(['foamDictionary ' + self.pwd + '/../_2_simulation/system/caseProperties -entry "' + key + '" -value'], shell=True, encoding='utf_8', stdout=subprocess.PIPE)
        _stdout = foam_dict.stdout

        if re.match("^[+\-]?[0-9]", _stdout) == None:
            # print("value type : str")
            value = _stdout.replace('\n', '').replace('"', '')
        elif re.match("^[+\-]?[0-9]+\.", _stdout) == None:
            # print("value type : int")
            value = int(_stdout)
        else:
            # print("value type : float")
            value = float(_stdout)

        return value


    def get_mean(self, dat_file):
        n = 100
        mean = 0
        for i in range(-n,0):
            # last_row = dat_file.iloc[i]
            # print(last_row)
            last_row = dat_file.iloc[i, -1]
            # print(last_row)
            values = last_row.split("\t")
            # print(values)
            mean += float(values[-1])
        mean /= n
        # print(mean)

        return mean


    def get_mean_velocities(self):
        file_u_in = pd.read_csv(self.pwd + "/../_2_simulation/postProcessing/meanVelocityIn/" + str(self.end_time) + "/surfaceFieldValue.dat", skiprows=4)
        u_in = self.get_mean(file_u_in)
        u_in = -u_in # it's because the direction of surface normal is negative X
        print("u_in = "+str(u_in))

        file_u_out = pd.read_csv(self.pwd + "/../_2_simulation/postProcessing/meanVelocityOut/" + str(self.end_time) + "/surfaceFieldValue.dat", skiprows=4)
        u_out = self.get_mean(file_u_out)
        print("u_out = "+str(u_out))

        return u_in, u_out


    def get_pressure_drop(self):
        file_pressure_drop = pd.read_csv(self.pwd + "/../_2_simulation/postProcessing/pressureDrop/" + str(self.end_time) + "/multiFieldValue.dat", skiprows=4)
        # "/pressureDrop" is dictionary name in "_2_simulation/system/funcMultiFieldValue"
        pressure_drop = self.get_mean(file_pressure_drop)

        return pressure_drop


    def compute_pressure_drop_coefficient(self):
        u_1, u_2 =  self.get_mean_velocities()
        dp_actual = self.get_pressure_drop()
        # dp_ideal = 0.5 * self.rho * (u_1**2 - u_2**2)
        dp_ideal = 0.0
        xi = (dp_ideal - dp_actual) / (0.5 * self.rho * (u_1 - u_2)**2)

        return xi


# somehow this content will be executed when imported. WHY!?
if __name__ == '__main__':
    print("begin running directly")
    e = c_extract_values()
    xi = e.compute_pressure_drop_coefficient()
    print(xi)
    print("end running directly")
