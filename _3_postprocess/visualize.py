# trace generated using paraview version 5.10.0
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
import os
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
pwd = __file__
print("pwd="+pwd)
dir_3_visualize = os.path.dirname(pwd)
dir_root = os.path.dirname(dir_3_visualize)
file_path = dir_root + '/_2_simulation/paraview.foam'
paraviewfoam = OpenFOAMReader(registrationName='paraview.foam', FileName=file_path)
paraviewfoam.MeshRegions = ['internalMesh']

# Properties modified on paraviewfoam
paraviewfoam.SkipZeroTime = 0
paraviewfoam.CaseType = 'Decomposed Case'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
paraviewfoamDisplay = Show(paraviewfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')

# trace defaults for the display properties.
paraviewfoamDisplay.Representation = 'Surface'
paraviewfoamDisplay.ColorArrayName = ['POINTS', 'p']
paraviewfoamDisplay.LookupTable = pLUT
paraviewfoamDisplay.SelectTCoordArray = 'None'
paraviewfoamDisplay.SelectNormalArray = 'None'
paraviewfoamDisplay.SelectTangentArray = 'None'
paraviewfoamDisplay.OSPRayScaleArray = 'p'
paraviewfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
paraviewfoamDisplay.SelectOrientationVectors = 'U'
paraviewfoamDisplay.ScaleFactor = 0.02750000059604645
paraviewfoamDisplay.SelectScaleArray = 'p'
paraviewfoamDisplay.GlyphType = 'Arrow'
paraviewfoamDisplay.GlyphTableIndexArray = 'p'
paraviewfoamDisplay.GaussianRadius = 0.0013750000298023224
paraviewfoamDisplay.SetScaleArray = ['POINTS', 'p']
paraviewfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
paraviewfoamDisplay.OpacityArray = ['POINTS', 'p']
paraviewfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
paraviewfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
paraviewfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
paraviewfoamDisplay.ScalarOpacityFunction = pPWF
paraviewfoamDisplay.ScalarOpacityUnitDistance = 0.007432917509250162
paraviewfoamDisplay.OpacityArrayName = ['POINTS', 'p']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
paraviewfoamDisplay.ScaleTransferFunction.Points = [-10.752519607543945, 0.0, 0.5, 0.0, 6.646039962768555, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
paraviewfoamDisplay.OpacityTransferFunction.Points = [-10.752519607543945, 0.0, 0.5, 0.0, 6.646039962768555, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera(False)

# show color bar/color legend
paraviewfoamDisplay.SetScalarBarVisibility(renderView1, True)

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# reset view to fit data
renderView1.ResetCamera(True)

# set scalar coloring
ColorBy(paraviewfoamDisplay, ('CELLS', 'U', 'Magnitude'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(pLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
paraviewfoamDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
paraviewfoamDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')

animationScene1.GoToLast()

# Rescale transfer function
uLUT.RescaleTransferFunction(0.0, 2.5)

# Rescale transfer function
uPWF.RescaleTransferFunction(0.0, 2.5)

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1920, 1080)

# current camera placement for renderView1
renderView1.CameraPosition = [0.0625, -1.2, 0.0375]
renderView1.CameraFocalPoint = [0.0625, 0.0375, 0.0375]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraViewAngle = 10.0
renderView1.CameraParallelScale = 0.14

# save screenshot
output_file_path = dir_3_visualize + '/U.png'
SaveScreenshot(output_file_path, renderView1, ImageResolution=[1920, 1080])
